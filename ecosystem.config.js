module.exports = {
    apps: [{
        script: 'main.py',
        interpreter: '/home/ubuntu/projects/cicd-gitlab/venv/bin/python',
        name: 'CICD flask app',
        cwd: '/home/ubuntu/projects/cicd-gitlab',
        env: {
            "client_name": "CICD-flask-app"
        }
    }]
};