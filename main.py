from flask import Flask

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index():
    return "CICD Executed Successfully disable"


def get_app():
    return app


def run_app():
    app.run(debug=False, threaded=True, port=5000)


if __name__ == '__main__':
    run_app()
